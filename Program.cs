﻿using System;

namespace ArrayChange
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static int arrayChange(int[] inputArray) {
            int outputInt = 0;
            for(int i = 0; i < inputArray.Length - 1; i++){
                if(inputArray[i] >= inputArray[i + 1]){
                    int howMuch = 0;
                    
                    if(inputArray[i] - inputArray[i + 1] == 0){
                        outputInt++;
                        inputArray[i + 1] += 1;
                        continue;
                    }else{
                        howMuch = inputArray[i] - inputArray[i + 1];
                        outputInt += (howMuch + 1);
                        inputArray[i + 1] += (howMuch + 1);
                        continue;
                    }
                }
            }
            return outputInt;
        }

    }

    
}
